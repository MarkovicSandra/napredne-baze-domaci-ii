﻿namespace NPB_Cassandra
{
    partial class DodajVoznju
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPolaziste = new System.Windows.Forms.TextBox();
            this.textBoxOdrediste = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBoxVozac = new System.Windows.Forms.ComboBox();
            this.comboBoxVozilo = new System.Windows.Forms.ComboBox();
            this.textBoxCena = new System.Windows.Forms.TextBox();
            this.buttonDodajVoznju = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Startna adresa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Krajnja adresa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cena";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Vreme";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Vozac";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 199);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Vozilo";
            // 
            // textBoxPolaziste
            // 
            this.textBoxPolaziste.Location = new System.Drawing.Point(107, 43);
            this.textBoxPolaziste.Name = "textBoxPolaziste";
            this.textBoxPolaziste.Size = new System.Drawing.Size(196, 20);
            this.textBoxPolaziste.TabIndex = 6;
            // 
            // textBoxOdrediste
            // 
            this.textBoxOdrediste.Location = new System.Drawing.Point(107, 74);
            this.textBoxOdrediste.Name = "textBoxOdrediste";
            this.textBoxOdrediste.Size = new System.Drawing.Size(196, 20);
            this.textBoxOdrediste.TabIndex = 7;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(107, 134);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(196, 20);
            this.dateTimePicker1.TabIndex = 8;
            // 
            // comboBoxVozac
            // 
            this.comboBoxVozac.FormattingEnabled = true;
            this.comboBoxVozac.Location = new System.Drawing.Point(107, 163);
            this.comboBoxVozac.Name = "comboBoxVozac";
            this.comboBoxVozac.Size = new System.Drawing.Size(196, 21);
            this.comboBoxVozac.TabIndex = 9;
            // 
            // comboBoxVozilo
            // 
            this.comboBoxVozilo.FormattingEnabled = true;
            this.comboBoxVozilo.Location = new System.Drawing.Point(107, 196);
            this.comboBoxVozilo.Name = "comboBoxVozilo";
            this.comboBoxVozilo.Size = new System.Drawing.Size(196, 21);
            this.comboBoxVozilo.TabIndex = 10;
            // 
            // textBoxCena
            // 
            this.textBoxCena.Location = new System.Drawing.Point(107, 104);
            this.textBoxCena.Name = "textBoxCena";
            this.textBoxCena.Size = new System.Drawing.Size(74, 20);
            this.textBoxCena.TabIndex = 11;
            // 
            // buttonDodajVoznju
            // 
            this.buttonDodajVoznju.Location = new System.Drawing.Point(137, 305);
            this.buttonDodajVoznju.Name = "buttonDodajVoznju";
            this.buttonDodajVoznju.Size = new System.Drawing.Size(75, 23);
            this.buttonDodajVoznju.TabIndex = 12;
            this.buttonDodajVoznju.Text = "Sacuvaj";
            this.buttonDodajVoznju.UseVisualStyleBackColor = true;
            this.buttonDodajVoznju.Click += new System.EventHandler(this.buttonDodajVoznju_Click);
            // 
            // DodajVoznju
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 376);
            this.Controls.Add(this.buttonDodajVoznju);
            this.Controls.Add(this.textBoxCena);
            this.Controls.Add(this.comboBoxVozilo);
            this.Controls.Add(this.comboBoxVozac);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.textBoxOdrediste);
            this.Controls.Add(this.textBoxPolaziste);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DodajVoznju";
            this.Text = "DodajVoznju";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPolaziste;
        private System.Windows.Forms.TextBox textBoxOdrediste;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBoxVozac;
        private System.Windows.Forms.ComboBox comboBoxVozilo;
        private System.Windows.Forms.TextBox textBoxCena;
        private System.Windows.Forms.Button buttonDodajVoznju;
    }
}