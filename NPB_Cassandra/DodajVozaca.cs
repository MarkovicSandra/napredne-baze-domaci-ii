﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Cassandra.DataLayer;
using NPB_Cassandra.DataLayer.QueryEntities;

namespace NPB_Cassandra
{
    public partial class DodajVozaca : Form
    {
        public DodajVozaca()
        {
            InitializeComponent();

            var vozaci = DataProvider.VratiVozace();

            foreach (var vozac in vozaci)
                listViewVozaci.Items.Add(new ListViewItem(vozac.ToString()));
        }

        private void buttonDodajVozaca_Click(object sender, EventArgs e)
        {
            string ime = textBoxIme.Text;
            string prezime = textBoxPrezime.Text;
            string email = textBoxEmail.Text;

            if (!Vozac.ProveriObaveznaPolja(ime, prezime, email))
                return;

            if ((bool)DataProvider.ProveriPostojecegVozaca(email))
            {
                MessageBox.Show("Vozac sa datim emailom postoji!");
                return;
            }

            string adresa = textBoxAdresa.Text;
            string telefon = textBoxTelefon.Text;
            DateTime datumRodjenja = dateTimePicker1.Value;

            Vozac vozac = new Vozac(ime, prezime, email, adresa, telefon, datumRodjenja);

            DataProvider.DodajVozaca(vozac);

            MessageBox.Show("Uspesno ste dodali vozaca!");

            var vozaci = DataProvider.VratiVozace();

            listViewVozaci.Items.Clear();

            foreach (var vozacc in vozaci)
                listViewVozaci.Items.Add(new ListViewItem(vozacc.ToString()));

            textBoxIme.Text = null;
            textBoxPrezime.Text = null;
            textBoxEmail.Text = null;
            textBoxAdresa.Text = null;
            textBoxTelefon.Text = null;
            dateTimePicker1.Value = DateTime.Now;
        }

        private void buttonObrisiVozaca_Click(object sender, EventArgs e)
        {
            var izabraniVozac = listViewVozaci.FocusedItem.Text;

            if (izabraniVozac == null)
            {
                MessageBox.Show("Morate izabrati vozaca za brisanje!");
                return;
            }

            Vozac vozac = Vozac.SelektovanVozac(izabraniVozac);

            DataProvider.ObrisiVozaca(vozac.Email);

            listViewVozaci.Items.Remove(listViewVozaci.FocusedItem);
        }
    }
}
