﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Cassandra.DataLayer;
using NPB_Cassandra.DataLayer.QueryEntities;

namespace NPB_Cassandra
{
    public partial class DodajVoznju : Form
    {
        public DodajVoznju()
        {
            InitializeComponent();
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "MM/dd/yyyy hh:mm";
            PopuniComboboxeve();
        }

        public void PopuniComboboxeve()
        {
            List<Vozac> vozaci = DataProvider.VratiVozace();
            List<Vozilo> vozila = DataProvider.VratiVozila();

            foreach (Vozac vozac in vozaci)
                comboBoxVozac.Items.Add(vozac.ToString());

            foreach (Vozilo vozilo in vozila)
                comboBoxVozilo.Items.Add(vozilo.ToString());
        }

        private void buttonDodajVoznju_Click(object sender, EventArgs e)
        {
            string polaziste = textBoxPolaziste.Text;
            string odrediste = textBoxOdrediste.Text;
            DateTime vreme = dateTimePicker1.Value;
            string cena = textBoxCena.Text;
            var vozac = comboBoxVozac.SelectedItem;
            var vozilo = comboBoxVozilo.SelectedItem;

            if (!Voznja.ProveriPolja(polaziste, odrediste, cena, vreme, vozilo, vozac))
            {
                return;
            }

            Vozilo izabranoVozilo = Vozilo.SelektovanoVozilo(vozilo.ToString());
            Vozac izabranVozac = Vozac.SelektovanVozac(vozac.ToString());

            Voznja voznja= new Voznja(polaziste, odrediste, Int32.Parse(cena), vreme, izabranoVozilo.Tablice, izabranVozac.Email);

            DataProvider.DodajVoznju(voznja);

            MessageBox.Show("Uspesno ste dodali voznju!");

            this.Close();
            
        }
    }
}
