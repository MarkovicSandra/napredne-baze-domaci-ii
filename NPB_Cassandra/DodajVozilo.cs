﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Cassandra.DataLayer;
using NPB_Cassandra.DataLayer.QueryEntities;

namespace NPB_Cassandra
{
    public partial class DodajVozilo : Form
    {
        public DodajVozilo()
        {
            InitializeComponent();

            var vozila = DataProvider.VratiVozila();

            foreach (var vozilo in vozila)
                listViewVozila.Items.Add(new ListViewItem(vozilo.ToString()));
        }

        private void buttonDodajVozilo_Click(object sender, EventArgs e)
        {
            string tablice = textBoxTablice.Text;
            string marka = textBoxMarka.Text;
            string model = textBoxModel.Text;

            if (!Vozilo.ProveriObaveznaPolja(tablice, marka, model))
                return;

            var provera = DataProvider.ProveriPostojeceVozilo(tablice);

            if ((bool)provera)
            {
                MessageBox.Show("Postoji vozilo sa datim tablicama!");
                return;
            }

            Vozilo vozilo = new Vozilo(tablice, marka, model);

            DataProvider.DodajVozilo(vozilo);

            MessageBox.Show("Uspesno ste dodali vozilo");

            var vozila = DataProvider.VratiVozila();

            listViewVozila.Items.Clear();

            foreach (var auto in vozila)
                listViewVozila.Items.Add(new ListViewItem(auto.ToString()));

            textBoxTablice.Text = null;
            textBoxMarka.Text = null;
            textBoxModel.Text = null;

        }

        private void buttonObrisiVozilo_Click(object sender, EventArgs e)
        {

            if (listViewVozila.FocusedItem == null)
            {
                MessageBox.Show("Morate izabrati vozilo za brisanje!");
                return;
            }

            var izabranoVozilo = listViewVozila.FocusedItem.Text;

            Vozilo vozilo = Vozilo.SelektovanoVozilo(izabranoVozilo);

            DataProvider.ObrisiVozilo(vozilo.Tablice);

            listViewVozila.Items.Remove(listViewVozila.FocusedItem);
        }


    }
}
