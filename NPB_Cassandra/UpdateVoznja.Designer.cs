﻿namespace NPB_Cassandra
{
    partial class UpdateVoznja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelID = new System.Windows.Forms.Label();
            this.buttonAzurirajVoznju = new System.Windows.Forms.Button();
            this.textBoxPolaziste = new System.Windows.Forms.TextBox();
            this.textBoxOdrediste = new System.Windows.Forms.TextBox();
            this.textBoxCena = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBoxVozac = new System.Windows.Forms.ComboBox();
            this.comboBoxVozilo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Startna adresa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Krajnja adresa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Cena";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Vreme";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Vozac";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 214);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Vozilo";
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(126, 40);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(0, 13);
            this.labelID.TabIndex = 7;
            // 
            // buttonAzurirajVoznju
            // 
            this.buttonAzurirajVoznju.Location = new System.Drawing.Point(145, 306);
            this.buttonAzurirajVoznju.Name = "buttonAzurirajVoznju";
            this.buttonAzurirajVoznju.Size = new System.Drawing.Size(84, 23);
            this.buttonAzurirajVoznju.TabIndex = 8;
            this.buttonAzurirajVoznju.Text = "Azuriraj";
            this.buttonAzurirajVoznju.UseVisualStyleBackColor = true;
            this.buttonAzurirajVoznju.Click += new System.EventHandler(this.buttonAzurirajVoznju_Click);
            // 
            // textBoxPolaziste
            // 
            this.textBoxPolaziste.Location = new System.Drawing.Point(129, 65);
            this.textBoxPolaziste.Name = "textBoxPolaziste";
            this.textBoxPolaziste.Size = new System.Drawing.Size(200, 20);
            this.textBoxPolaziste.TabIndex = 9;
            // 
            // textBoxOdrediste
            // 
            this.textBoxOdrediste.Location = new System.Drawing.Point(129, 91);
            this.textBoxOdrediste.Name = "textBoxOdrediste";
            this.textBoxOdrediste.Size = new System.Drawing.Size(200, 20);
            this.textBoxOdrediste.TabIndex = 10;
            // 
            // textBoxCena
            // 
            this.textBoxCena.Location = new System.Drawing.Point(129, 118);
            this.textBoxCena.Name = "textBoxCena";
            this.textBoxCena.Size = new System.Drawing.Size(100, 20);
            this.textBoxCena.TabIndex = 11;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(129, 145);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 12;
            // 
            // comboBoxVozac
            // 
            this.comboBoxVozac.FormattingEnabled = true;
            this.comboBoxVozac.Location = new System.Drawing.Point(129, 179);
            this.comboBoxVozac.Name = "comboBoxVozac";
            this.comboBoxVozac.Size = new System.Drawing.Size(200, 21);
            this.comboBoxVozac.TabIndex = 13;
            // 
            // comboBoxVozilo
            // 
            this.comboBoxVozilo.FormattingEnabled = true;
            this.comboBoxVozilo.Location = new System.Drawing.Point(129, 211);
            this.comboBoxVozilo.Name = "comboBoxVozilo";
            this.comboBoxVozilo.Size = new System.Drawing.Size(200, 21);
            this.comboBoxVozilo.TabIndex = 14;
            // 
            // UpdateVoznja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 375);
            this.Controls.Add(this.comboBoxVozilo);
            this.Controls.Add(this.comboBoxVozac);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.textBoxCena);
            this.Controls.Add(this.textBoxOdrediste);
            this.Controls.Add(this.textBoxPolaziste);
            this.Controls.Add(this.buttonAzurirajVoznju);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UpdateVoznja";
            this.Text = "Voznja";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.Button buttonAzurirajVoznju;
        private System.Windows.Forms.TextBox textBoxPolaziste;
        private System.Windows.Forms.TextBox textBoxOdrediste;
        private System.Windows.Forms.TextBox textBoxCena;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBoxVozac;
        private System.Windows.Forms.ComboBox comboBoxVozilo;
    }
}