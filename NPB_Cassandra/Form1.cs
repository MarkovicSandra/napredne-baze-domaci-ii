﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Cassandra.DataLayer.QueryEntities;
using NPB_Cassandra.DataLayer;

namespace NPB_Cassandra
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            PopuniListuVoznji();
            labelPretraga1.Visible = false;
            textBoxPretraga1.Visible = false;
            labelPretraga2.Visible = false;
            textBoxPretraga2.Visible = false;
            dateTimePickerPretraga1.Format = DateTimePickerFormat.Custom;
            dateTimePickerPretraga1.CustomFormat = "MM/dd/yyyy hh:mm";
            dateTimePickerPretraga2.Format = DateTimePickerFormat.Custom;
            dateTimePickerPretraga2.CustomFormat = "MM/dd/yyyy hh:mm";
            dateTimePickerPretraga1.Visible = false;
            dateTimePickerPretraga2.Visible = false;
        }

        private void PopuniListuVoznji()
        {
            var voznje = DataProvider.VratiVoznje();

            voznje = voznje.Select(x => x).OrderBy(x => x.ID).ToList();

            foreach (var voznja in voznje)
                listViewVoznje.Items.Add(new ListViewItem(voznja.ToString()));
        }

        private void buttonDodajVoznju_Click(object sender, EventArgs e)
        {
            var form = new DodajVoznju();
            form.Show();
        }

        private void buttonDodajVozaca_Click(object sender, EventArgs e)
        {
            var form = new DodajVozaca();
            form.Show();
        }

        private void buttonDodajVozilo_Click(object sender, EventArgs e)
        {
            var form = new DodajVozilo();
            form.Show();
        }

        private void listViewVoznje_ItemActivate(object sender, EventArgs e)
        {
            string izabranaVoznja = listViewVoznje.FocusedItem.Text;

            string id = izabranaVoznja.Substring(0,1);

            int ID = Convert.ToInt32(id);

            var voznja = DataProvider.VratiVoznju(ID);

            var form = new UpdateVoznja(voznja);

            form.Show();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            listViewVoznje.Items.Clear();
            PopuniListuVoznji();
        }

        private void buttonObrisiVoznju_Click(object sender, EventArgs e)
        {
            var izabranaVoznja = listViewVoznje.FocusedItem;

            if (izabranaVoznja == null)
            {
                MessageBox.Show("Morate izabrati voznju za brisanje!");

                return;
            }

            string id = izabranaVoznja.Text.Substring(0, 1);

            int ID = Convert.ToInt32(id);

            DataProvider.ObrisiVoznju(ID);

            listViewVoznje.Items.Remove(izabranaVoznja);
        }

        private void comboBoxPretraga_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxPretraga.SelectedIndex == 0)
            {
                labelPretraga1.Visible = true;
                labelPretraga1.Text = "Email:";
                textBoxPretraga1.Visible = true;
                labelPretraga2.Visible = false;
                textBoxPretraga2.Visible = false;
                dateTimePickerPretraga1.Visible = false;
                dateTimePickerPretraga2.Visible = false;
            }

            if (comboBoxPretraga.SelectedIndex == 1)
            {
                labelPretraga1.Visible = true;
                labelPretraga2.Visible = true;
                labelPretraga1.Text = "Od:";
                labelPretraga2.Text = "Do:";
                textBoxPretraga1.Visible = true;
                textBoxPretraga2.Visible = true;
                dateTimePickerPretraga1.Visible = false;
                dateTimePickerPretraga2.Visible = false;
            }

            if (comboBoxPretraga.SelectedIndex == 2)
            {
                labelPretraga1.Visible = true;
                labelPretraga2.Visible = true;
                labelPretraga1.Text = "Od:";
                labelPretraga2.Text = "Do:";
                textBoxPretraga1.Visible = false;
                textBoxPretraga2.Visible = false;
                dateTimePickerPretraga1.Visible = true;
                dateTimePickerPretraga2.Visible = true;
            }

            if (comboBoxPretraga.SelectedIndex == 3)
            {
                labelPretraga1.Visible = true;
                labelPretraga2.Visible = false;
                labelPretraga1.Text = "Tablice:";
                textBoxPretraga1.Visible = true;
                textBoxPretraga2.Visible = false;
                dateTimePickerPretraga1.Visible = false;
                dateTimePickerPretraga2.Visible = false;
            }

            if (comboBoxPretraga.SelectedIndex == 4)
            {
                labelPretraga1.Visible = true;
                labelPretraga1.Text = "Adresa:";
                textBoxPretraga1.Visible = true;
                labelPretraga2.Visible = false;
                textBoxPretraga2.Visible = false;
                dateTimePickerPretraga1.Visible = false;
                dateTimePickerPretraga2.Visible = false;
            }

            if (comboBoxPretraga.SelectedIndex == 5)
            {
                labelPretraga1.Visible = true;
                labelPretraga1.Text = "Adresa:";
                textBoxPretraga1.Visible = true;
                labelPretraga2.Visible = false;
                textBoxPretraga2.Visible = false;
                dateTimePickerPretraga1.Visible = false;
                dateTimePickerPretraga2.Visible = false;
            }
        }

        private void buttonPretrazi_Click(object sender, EventArgs e)
        {
            if (comboBoxPretraga.SelectedIndex == 0)
            {
                if (!ProveriPoljeZaPretragu(textBoxPretraga1.Text))
                {
                    return;
                }

                var voznje = DataProvider.PretraziVoznjePoVozacu(textBoxPretraga1.Text);
               
                listViewVoznje.Items.Clear();
               
                foreach (var voznja in voznje)
                {
                    listViewVoznje.Items.Add(new ListViewItem(voznja.ToString()));
                }
               
                return;

            }

            if (comboBoxPretraga.SelectedIndex == 1)
            {
                if (!ProveriCeneZaPretragu(textBoxPretraga1.Text, textBoxPretraga2.Text))
                {
                    return;
                }

                var voznje = DataProvider.PretraziVoznjePoCeni(textBoxPretraga1.Text, textBoxPretraga2.Text);

                listViewVoznje.Items.Clear();

                foreach (var voznja in voznje)
                {
                    listViewVoznje.Items.Add(new ListViewItem(voznja.ToString()));
                }

                return;
            }

            if (comboBoxPretraga.SelectedIndex == 2)
            {
                if (!ProveriVremeZaPretragu(dateTimePickerPretraga1.Value, dateTimePickerPretraga2.Value))
                {
                    return;
                }

                var voznje = DataProvider.PretraziVoznjePoVremenu(dateTimePickerPretraga1.Value, dateTimePickerPretraga2.Value);

                listViewVoznje.Items.Clear();

                foreach (var voznja in voznje)
                {
                    listViewVoznje.Items.Add(new ListViewItem(voznja.ToString()));
                }

                return;
            }

            if (comboBoxPretraga.SelectedIndex == 3)
            {
                if (!ProveriPoljeZaPretragu(textBoxPretraga1.Text))
                {
                    return;
                }

                var voznje = DataProvider.PretraziVoznjePoVozilu(textBoxPretraga1.Text);

                listViewVoznje.Items.Clear();

                foreach (var voznja in voznje)
                {
                    listViewVoznje.Items.Add(new ListViewItem(voznja.ToString()));
                }

                return;
            }

            if (comboBoxPretraga.SelectedIndex == 4)
            {
                if (!ProveriPoljeZaPretragu(textBoxPretraga1.Text))
                {
                    return;
                }

                var voznje = DataProvider.PretraziVoznjePoAdresi(textBoxPretraga1.Text, true);

                listViewVoznje.Items.Clear();

                foreach (var voznja in voznje)
                {
                    listViewVoznje.Items.Add(new ListViewItem(voznja.ToString()));
                }

                return;
            }

            if (comboBoxPretraga.SelectedIndex == 5)
            {
                if (!ProveriPoljeZaPretragu(textBoxPretraga1.Text))
                {
                    return;
                }

                var voznje = DataProvider.PretraziVoznjePoAdresi(textBoxPretraga1.Text, false);

                listViewVoznje.Items.Clear();

                foreach (var voznja in voznje)
                {
                    listViewVoznje.Items.Add(new ListViewItem(voznja.ToString()));
                }

                return;
            }
        }

        private bool ProveriPoljeZaPretragu(string poljeZaPretragu)
        {
            if (string.IsNullOrWhiteSpace(poljeZaPretragu))
            {
                MessageBox.Show("Morate uneti polje za pretragu");
                return false;
            }

            return true;
        }

        private bool ProveriCeneZaPretragu(string min, string max)
        {
            if (string.IsNullOrWhiteSpace(min) || string.IsNullOrWhiteSpace(max))
            {
                MessageBox.Show("Morate uneti oba polja za pretragu");
                return false;
            }

            int manja;
            int veca;
            bool proveraParsiranjaManja = Int32.TryParse(min, out manja);
            bool proveraParsiranjaVeca = Int32.TryParse(min, out veca);

            if (!proveraParsiranjaManja || !proveraParsiranjaVeca)
            {
                MessageBox.Show("Cena mora biti broj");
                return false;
            }

            if (manja <= 0 || veca <= 0)
            {
                MessageBox.Show("Cena mora biti veca od 0 din");
                return false;
            }

            return true;
        }

        private bool ProveriVremeZaPretragu(DateTime pocetak, DateTime kraj)
        {
            if (pocetak == null || pocetak == default(DateTime) ||  kraj == null || kraj == default(DateTime))
            {
                MessageBox.Show("Morate uneti oba polja za pretragu");
                return false;
            }

            return true;
        }
    }
}
