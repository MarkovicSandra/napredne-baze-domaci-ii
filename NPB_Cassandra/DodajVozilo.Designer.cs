﻿namespace NPB_Cassandra
{
    partial class DodajVozilo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxTablice = new System.Windows.Forms.TextBox();
            this.textBoxMarka = new System.Windows.Forms.TextBox();
            this.textBoxModel = new System.Windows.Forms.TextBox();
            this.buttonDodajVozilo = new System.Windows.Forms.Button();
            this.listViewVozila = new System.Windows.Forms.ListView();
            this.buttonObrisiVozilo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registarske tablice";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ko";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Model";
            // 
            // textBoxTablice
            // 
            this.textBoxTablice.Location = new System.Drawing.Point(123, 34);
            this.textBoxTablice.Name = "textBoxTablice";
            this.textBoxTablice.Size = new System.Drawing.Size(100, 20);
            this.textBoxTablice.TabIndex = 3;
            // 
            // textBoxMarka
            // 
            this.textBoxMarka.Location = new System.Drawing.Point(123, 68);
            this.textBoxMarka.Name = "textBoxMarka";
            this.textBoxMarka.Size = new System.Drawing.Size(100, 20);
            this.textBoxMarka.TabIndex = 4;
            // 
            // textBoxModel
            // 
            this.textBoxModel.Location = new System.Drawing.Point(123, 102);
            this.textBoxModel.Name = "textBoxModel";
            this.textBoxModel.Size = new System.Drawing.Size(100, 20);
            this.textBoxModel.TabIndex = 5;
            // 
            // buttonDodajVozilo
            // 
            this.buttonDodajVozilo.Location = new System.Drawing.Point(123, 180);
            this.buttonDodajVozilo.Name = "buttonDodajVozilo";
            this.buttonDodajVozilo.Size = new System.Drawing.Size(75, 23);
            this.buttonDodajVozilo.TabIndex = 6;
            this.buttonDodajVozilo.Text = "Sacuvaj";
            this.buttonDodajVozilo.UseVisualStyleBackColor = true;
            this.buttonDodajVozilo.Click += new System.EventHandler(this.buttonDodajVozilo_Click);
            // 
            // listViewVozila
            // 
            this.listViewVozila.HideSelection = false;
            this.listViewVozila.Location = new System.Drawing.Point(283, 34);
            this.listViewVozila.MultiSelect = false;
            this.listViewVozila.Name = "listViewVozila";
            this.listViewVozila.Size = new System.Drawing.Size(280, 130);
            this.listViewVozila.TabIndex = 7;
            this.listViewVozila.UseCompatibleStateImageBehavior = false;
            this.listViewVozila.View = System.Windows.Forms.View.List;
            // 
            // buttonObrisiVozilo
            // 
            this.buttonObrisiVozilo.Location = new System.Drawing.Point(392, 180);
            this.buttonObrisiVozilo.Name = "buttonObrisiVozilo";
            this.buttonObrisiVozilo.Size = new System.Drawing.Size(75, 23);
            this.buttonObrisiVozilo.TabIndex = 8;
            this.buttonObrisiVozilo.Text = "Obrisi vozilo";
            this.buttonObrisiVozilo.UseVisualStyleBackColor = true;
            this.buttonObrisiVozilo.Click += new System.EventHandler(this.buttonObrisiVozilo_Click);
            // 
            // DodajVozilo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 235);
            this.Controls.Add(this.buttonObrisiVozilo);
            this.Controls.Add(this.listViewVozila);
            this.Controls.Add(this.buttonDodajVozilo);
            this.Controls.Add(this.textBoxModel);
            this.Controls.Add(this.textBoxMarka);
            this.Controls.Add(this.textBoxTablice);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DodajVozilo";
            this.Text = "DodajVozilo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxTablice;
        private System.Windows.Forms.TextBox textBoxMarka;
        private System.Windows.Forms.TextBox textBoxModel;
        private System.Windows.Forms.Button buttonDodajVozilo;
        private System.Windows.Forms.ListView listViewVozila;
        private System.Windows.Forms.Button buttonObrisiVozilo;
    }
}