﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra;
using NPB_Cassandra.DataLayer.QueryEntities;

namespace NPB_Cassandra.DataLayer
{
    public static class DataProvider
    {
        public static List<Vozac> VratiVozace()
        {
            ISession session = SessionManager.GetSession();
            List<Vozac> vozaci = new List<Vozac>();

            if (session == null)
                return null;

            RowSet vozacData = session.Execute("select * from \"Vozac\"");

            foreach (var vozacd in vozacData)
            {
                if (vozacd != null)
                {
                    Vozac vozac = new Vozac(vozacd);
                    vozaci.Add(vozac);
                }
            }

            return vozaci;
        }

        public static List<Voznja> VratiVoznje()
        {
            ISession session = SessionManager.GetSession();
            List<Voznja> voznje = new List<Voznja>();

            if (session == null)
                return null;

            RowSet voznjaData = session.Execute("select * from \"Voznja\"");

            foreach (var voznjad in voznjaData)
            {
                if (voznjad != null)
                {
                    Voznja voznja = new Voznja(voznjad);
                    voznje.Add(voznja);
                }
            }

            return voznje;
        }

        public static List<Vozilo> VratiVozila()
        {
            ISession session = SessionManager.GetSession();
            List<Vozilo> vozila = new List<Vozilo>();

            if (session == null)
                return null;

            RowSet vozilaData = session.Execute("select * from \"Vozilo\"");

            foreach (var vozilod in vozilaData)
            {
                if (vozilod != null)
                {
                    Vozilo vozilo = new Vozilo(vozilod);
                    vozila.Add(vozilo);
                }
            }

            return vozila;
        }

        public static bool? ProveriPostojecegVozaca(string email)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            RowSet vozaci = session.Execute("select * from \"Vozac\" where \"Email\" = '" + email + "'");

            if (vozaci.Count() > 0)
            {
                return true;
            }

            return false;
        }

        public static bool? ProveriPostojeceVozilo(string tablice)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            RowSet vozila = session.Execute("select * from \"Vozilo\" where \"Tablice\" = '" + tablice + "'");

            if (vozila.Count() > 0)
            {
                return true;
            }

            return false;
        }

        public static int NadjiSledeciSlobodanID(ISession session)
        {
            Row idRed = session.Execute("select max(\"ID\") from \"Voznja\"").FirstOrDefault();

            string id_dogadjaja;

            if (!idRed.IsNull("system.max(ID)"))
                id_dogadjaja = idRed["system.max(ID)"].ToString();
            else id_dogadjaja = "0";

            Int32 idInt = Int32.Parse(id_dogadjaja);
            idInt++;

            return idInt;
        }

        public static void DodajVozaca(Vozac vozac)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var datum = vozac.DatumRodjenja.Value.Date.ToString("D");

            var query = "insert into \"Vozac\" (\"Email\", ime, prezime, adresa, telefon, datumrodjenja) values ('"
                + vozac.Email + "','" + vozac.Ime + "','" + vozac.Prezime + "','" + vozac.Adresa + "','" + vozac.Telefon
                + "','" + datum + "')";

            var result = session.Execute(query);
        }

        public static void DodajVozilo(Vozilo vozilo)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var query = "insert into \"Vozilo\" (\"Tablice\", marka, model) values ('"
                + vozilo.Tablice + "','" + vozilo.Marka + "','" + vozilo.Model + "')";

            var result = session.Execute(query);
        }

        public static void DodajVoznju(Voznja voznja)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var id = NadjiSledeciSlobodanID(session);
            voznja.ID = id;

            var vreme = voznja.Vreme.ToString("f");

            var query = "insert into \"Voznja\" (\"ID\", polaziste, odrediste, cena, vreme, vozilo, vozac) values ("
                + voznja.ID + ",'" + voznja.Polaziste + "','" + voznja.Odrediste + "'," + voznja.Cena
                + ",'" + vreme + "','" + voznja.Vozilo + "','" + voznja.Vozac + "')";

            var result = session.Execute(query);
        }

        public static Voznja VratiVoznju(int ID)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            Row voznja = session.Execute("select * from \"Voznja\" where \"ID\" = " + ID).FirstOrDefault();

            return new Voznja(voznja);
        }

        public static void ObrisiVoznju(int ID)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            session.Execute("delete from \"Voznja\" where \"ID\" = " + ID);
        }

        public static void ObrisiVozaca(string email)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var result = session.Execute("select * from \"Voznja\" where vozac = '" + email + "'");

            foreach (var row in result)
            {
                var voznja = new Voznja(row);
                ObrisiVozacaIzVoznje(voznja);
            }

            session.Execute("delete from \"Vozac\" where \"Email\" = '" + email + "'");
        }

        public static void ObrisiVozilo(string tablice)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var result = session.Execute("select * from \"Voznja\" where vozilo = '" + tablice + "'");

            foreach (var row in result)
            {
                var voznja = new Voznja(row);
                ObrisiVoziloIzVoznje(voznja);
            }

            session.Execute("delete from \"Vozilo\" where \"Tablice\" = '" + tablice + "'");
        }

        public static void IzmeniVoznju(Voznja voznja)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var vreme = voznja.Vreme.ToString("f");

            var query = "update \"Voznja\" set polaziste = '" + voznja.Polaziste
                + "', odrediste = '" + voznja.Odrediste + "', cena=" + voznja.Cena +
                ",vreme='" + vreme + "',vozilo='" + voznja.Vozilo + "',vozac='" + voznja.Vozac +
                "' where \"ID\" = " + voznja.ID;

            session.Execute(query);
        }

        public static void ObrisiVozacaIzVoznje(Voznja voznja)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var query = "update \"Voznja\" set vozac = NULL where \"ID\"=" + voznja.ID;

            session.Execute(query);
        }

        public static void ObrisiVoziloIzVoznje(Voznja voznja)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return;

            var query = "update \"Voznja\" set vozilo = NULL where \"ID\"=" + voznja.ID;

            session.Execute(query);
        }

        public static List<Voznja> PretraziVoznjePoVozacu(string email)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            var query2 = "select * from \"Voznja\" WHERE vozac = '" + email + "'";

            RowSet rowset = session.Execute(query2);

            var voznje = new List<Voznja>();
            foreach (var row in rowset)
            {
                Voznja voznja = new Voznja(row);

                voznje.Add(voznja);
            }

            return voznje;
        }

        public static List<Voznja> PretraziVoznjePoCeni(string donjaGranica, string gornjaGranica)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            var query2 = "select * from \"Voznja\" WHERE cena >= " + Convert.ToInt32(donjaGranica) +
                " and cena <= " + Convert.ToInt32(gornjaGranica) + " ALLOW FILTERING";

            RowSet rowset = session.Execute(query2);

            var voznje = new List<Voznja>();

            foreach (var row in rowset)
            {
                Voznja voznja = new Voznja(row);
                voznje.Add(voznja);
            }

            return voznje;
        }

        public static List<Voznja> PretraziVoznjePoVremenu(DateTime pocetak, DateTime kraj)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            var voznje = VratiVoznje();

            return voznje.Where(x => x.Vreme >= pocetak && x.Vreme <= kraj).ToList();
        }

        public static List<Voznja> PretraziVoznjePoVozilu(string tablice)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            var query2 = "select * from \"Voznja\" WHERE vozilo = '" + tablice  + "'";

            RowSet rowset = session.Execute(query2);

            var voznje = new List<Voznja>();
            foreach (var row in rowset)
            {
                Voznja voznja = new Voznja(row);

                voznje.Add(voznja);
            }

            return voznje;
        }

        public static List<Voznja> PretraziVoznjePoAdresi(string adresa, bool polaziste)
        {
            ISession session = SessionManager.GetSession();

            if (session == null)
                return null;

            string query;
            if (polaziste)
            {
                query = "select * from \"Voznja\" WHERE polaziste = '" + adresa + "'";
            }
            else
            {
                query = "select * from \"Voznja\" WHERE odrediste = '" + adresa + "'";
            }

            RowSet rowset = session.Execute(query);

            var voznje = new List<Voznja>();
            foreach (var row in rowset)
            {
                Voznja voznja = new Voznja(row);

                voznje.Add(voznja);
            }

            return voznje;
        }
    }
}
