﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cassandra;

namespace NPB_Cassandra.DataLayer.QueryEntities
{
    public class Vozac
    {
        public string Email { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Adresa { get; set; }
        public string Telefon { get; set; }
        public DateTime? DatumRodjenja { get; set; }

        public override string ToString()
        {
            return Ime + " " + Prezime + " " + Email;
        }

        public Vozac(Row vozac)
        {
            Email = Convert.ToString(vozac["Email"]);
            Ime = Convert.ToString(vozac["ime"]);
            Prezime = Convert.ToString(vozac["prezime"]);
            Adresa = Convert.ToString(vozac["adresa"]);
            Telefon = Convert.ToString(vozac["telefon"]);
            DatumRodjenja = Convert.ToDateTime(vozac["datumrodjenja"]);
        }

        public Vozac()
        { }

        public Vozac(string ime, string prezime, string email, string adresa, string telefon, DateTime datumRodjenja)
        {
            Ime = ime;
            Prezime = prezime;
            Email = email;
            Adresa = adresa;
            Telefon = telefon;
            DatumRodjenja = datumRodjenja;
        }

        public static bool ProveriObaveznaPolja(string ime, string prezime, string email)
        {
            if (string.IsNullOrWhiteSpace(ime) || string.IsNullOrWhiteSpace(prezime)
                || string.IsNullOrWhiteSpace(email))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
                return false;
            }

            return true;
        }

        public static Vozac SelektovanVozac(string vozac)
        {
            char[] separator = { ' ' };
            var parametri = vozac.Split(separator, 3);

            return new Vozac { Ime = parametri[0], Prezime = parametri[1], Email = parametri[2] };
        }
    }
}
