﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPB_Cassandra.DataLayer.QueryEntities
{
    public class Pacijent
    {
        public string ID { get; set; }
        public string adresa { get; set; }
        public string datumrodjenja { get; set; }
        public string ime { get; set; }
        public string jmbg { get; set; }
        public string prezime { get; set; }
        public string pol { get; set; }
        public string email { get; set; }

        public string telefon { get; set; }

        public override string ToString()
        {
            return ime + " " + prezime;
        }
    }
}
