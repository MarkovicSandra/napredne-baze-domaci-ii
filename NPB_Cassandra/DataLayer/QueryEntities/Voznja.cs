﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cassandra;

namespace NPB_Cassandra.DataLayer.QueryEntities
{
    public class Voznja
    {
        public int ID { get; set; }
        public string Polaziste { get; set; }
        public string Odrediste { get; set; }
        public int Cena { get; set; }
        public DateTime Vreme { get; set; }
        public string Vozilo { get; set; }
        public string Vozac { get; set; }

        public override string ToString()
        {
            return ID + " " + Polaziste + "-" + Odrediste + " " + Vreme.Date.ToString("d") + " " + Vreme.TimeOfDay;
        }

        public Voznja()
        { }

        public Voznja(Row voznja)
        {
            ID = Convert.ToInt32(voznja["ID"]);
            Polaziste = Convert.ToString(voznja["polaziste"]);
            Odrediste = Convert.ToString(voznja["odrediste"]);
            Cena = Convert.ToInt32(voznja["cena"]);
            Vreme = Convert.ToDateTime(voznja["vreme"]);
            Vozilo = Convert.ToString(voznja["vozilo"]);
            Vozac = Convert.ToString(voznja["vozac"]);
        }

        public Voznja(string polaziste, string odrediste, int cena, DateTime vreme, string vozilo, string vozac)
        {
            Polaziste = polaziste;
            Odrediste = odrediste;
            Vreme = vreme;
            Cena = cena;
            Vozilo = vozilo;
            Vozac = vozac;
        }

        public static bool ProveriPolja(string polaziste, string odrediste, string cena, DateTime vreme, object vozilo, object vozac)
        {
            if (string.IsNullOrWhiteSpace(polaziste) || string.IsNullOrWhiteSpace(odrediste)
                || string.IsNullOrWhiteSpace(cena)|| vreme == null || vreme == default(DateTime) || vozilo == null || vozac == null)
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
                return false;
            }

            int cenaa;
            bool proveraParsiranja = Int32.TryParse(cena, out cenaa);

            if (!proveraParsiranja)
            {
                MessageBox.Show("Cena mora biti broj");
                return false;
            }

            if (cenaa <= 0)
            {
                MessageBox.Show("Cena mora biti veca od 0 din");
                return false;
            }

            return true;
        }

    }
}
