﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cassandra;

namespace NPB_Cassandra.DataLayer.QueryEntities
{
    public class Vozilo
    {
        public string Tablice { get; set; }
        public string Marka { get; set; }
        public string Model { get; set; }

        public override string ToString()
        {
            return Tablice + " " + Marka + " " + Model;
        }

        public Vozilo()
        { }

        public Vozilo(Row vozilo)
        {
            Tablice = Convert.ToString(vozilo["Tablice"]);
            Marka = Convert.ToString(vozilo["marka"]);
            Model = Convert.ToString(vozilo["model"]);
        }

        public Vozilo(string tablice, string marka, string model)
        {
            Tablice = tablice;
            Marka = marka;
            Model = model;
        }

        public static bool ProveriObaveznaPolja(string tablice, string marka, string model)
        {
            if (string.IsNullOrWhiteSpace(tablice) || string.IsNullOrWhiteSpace(marka)
                || string.IsNullOrWhiteSpace(model))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
                return false;
            }

            return true;
        }

        public static Vozilo SelektovanoVozilo(string vozilo)
        {
            char[] separator = { ' ' };
            var parametri = vozilo.Split(separator, 3);

            return new Vozilo { Tablice = parametri[0], Marka = parametri[1], Model = parametri[2] };
        }
    }
}
