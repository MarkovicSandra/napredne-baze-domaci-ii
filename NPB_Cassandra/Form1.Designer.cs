﻿namespace NPB_Cassandra
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listViewVoznje = new System.Windows.Forms.ListView();
            this.buttonDodajVoznju = new System.Windows.Forms.Button();
            this.buttonDodajVozaca = new System.Windows.Forms.Button();
            this.buttonDodajVozilo = new System.Windows.Forms.Button();
            this.buttonObrisiVoznju = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerPretraga2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerPretraga1 = new System.Windows.Forms.DateTimePicker();
            this.textBoxPretraga2 = new System.Windows.Forms.TextBox();
            this.textBoxPretraga1 = new System.Windows.Forms.TextBox();
            this.labelPretraga2 = new System.Windows.Forms.Label();
            this.labelPretraga1 = new System.Windows.Forms.Label();
            this.comboBoxPretraga = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonPretrazi = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listViewVoznje);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(547, 273);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Voznje";
            // 
            // listViewVoznje
            // 
            this.listViewVoznje.HideSelection = false;
            this.listViewVoznje.Location = new System.Drawing.Point(6, 19);
            this.listViewVoznje.MultiSelect = false;
            this.listViewVoznje.Name = "listViewVoznje";
            this.listViewVoznje.Size = new System.Drawing.Size(535, 246);
            this.listViewVoznje.TabIndex = 0;
            this.listViewVoznje.UseCompatibleStateImageBehavior = false;
            this.listViewVoznje.View = System.Windows.Forms.View.List;
            this.listViewVoznje.ItemActivate += new System.EventHandler(this.listViewVoznje_ItemActivate);
            // 
            // buttonDodajVoznju
            // 
            this.buttonDodajVoznju.Location = new System.Drawing.Point(11, 31);
            this.buttonDodajVoznju.Name = "buttonDodajVoznju";
            this.buttonDodajVoznju.Size = new System.Drawing.Size(92, 35);
            this.buttonDodajVoznju.TabIndex = 1;
            this.buttonDodajVoznju.Text = "Dodaj voznju";
            this.buttonDodajVoznju.UseVisualStyleBackColor = true;
            this.buttonDodajVoznju.Click += new System.EventHandler(this.buttonDodajVoznju_Click);
            // 
            // buttonDodajVozaca
            // 
            this.buttonDodajVozaca.Location = new System.Drawing.Point(11, 93);
            this.buttonDodajVozaca.Name = "buttonDodajVozaca";
            this.buttonDodajVozaca.Size = new System.Drawing.Size(92, 37);
            this.buttonDodajVozaca.TabIndex = 2;
            this.buttonDodajVozaca.Text = "Dodaj ili obrisi vozaca";
            this.buttonDodajVozaca.UseVisualStyleBackColor = true;
            this.buttonDodajVozaca.Click += new System.EventHandler(this.buttonDodajVozaca_Click);
            // 
            // buttonDodajVozilo
            // 
            this.buttonDodajVozilo.Location = new System.Drawing.Point(11, 162);
            this.buttonDodajVozilo.Name = "buttonDodajVozilo";
            this.buttonDodajVozilo.Size = new System.Drawing.Size(92, 36);
            this.buttonDodajVozilo.TabIndex = 3;
            this.buttonDodajVozilo.Text = "Dodaj ili obrisi vozilo";
            this.buttonDodajVozilo.UseVisualStyleBackColor = true;
            this.buttonDodajVozilo.Click += new System.EventHandler(this.buttonDodajVozilo_Click);
            // 
            // buttonObrisiVoznju
            // 
            this.buttonObrisiVoznju.Location = new System.Drawing.Point(114, 301);
            this.buttonObrisiVoznju.Name = "buttonObrisiVoznju";
            this.buttonObrisiVoznju.Size = new System.Drawing.Size(126, 23);
            this.buttonObrisiVoznju.TabIndex = 4;
            this.buttonObrisiVoznju.Text = "Obrisi izabranu voznju";
            this.buttonObrisiVoznju.UseVisualStyleBackColor = true;
            this.buttonObrisiVoznju.Click += new System.EventHandler(this.buttonObrisiVoznju_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonDodajVozilo);
            this.groupBox2.Controls.Add(this.buttonDodajVozaca);
            this.groupBox2.Controls.Add(this.buttonDodajVoznju);
            this.groupBox2.Location = new System.Drawing.Point(18, 347);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(115, 218);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dodaj";
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(345, 301);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(105, 23);
            this.buttonRefresh.TabIndex = 6;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dateTimePickerPretraga2);
            this.groupBox3.Controls.Add(this.dateTimePickerPretraga1);
            this.groupBox3.Controls.Add(this.textBoxPretraga2);
            this.groupBox3.Controls.Add(this.textBoxPretraga1);
            this.groupBox3.Controls.Add(this.labelPretraga2);
            this.groupBox3.Controls.Add(this.labelPretraga1);
            this.groupBox3.Controls.Add(this.comboBoxPretraga);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.buttonPretrazi);
            this.groupBox3.Location = new System.Drawing.Point(155, 347);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(383, 218);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pretraga";
            // 
            // dateTimePickerPretraga2
            // 
            this.dateTimePickerPretraga2.Location = new System.Drawing.Point(95, 99);
            this.dateTimePickerPretraga2.Name = "dateTimePickerPretraga2";
            this.dateTimePickerPretraga2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerPretraga2.TabIndex = 10;
            // 
            // dateTimePickerPretraga1
            // 
            this.dateTimePickerPretraga1.Location = new System.Drawing.Point(95, 65);
            this.dateTimePickerPretraga1.Name = "dateTimePickerPretraga1";
            this.dateTimePickerPretraga1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerPretraga1.TabIndex = 9;
            // 
            // textBoxPretraga2
            // 
            this.textBoxPretraga2.Location = new System.Drawing.Point(95, 99);
            this.textBoxPretraga2.Name = "textBoxPretraga2";
            this.textBoxPretraga2.Size = new System.Drawing.Size(121, 20);
            this.textBoxPretraga2.TabIndex = 7;
            // 
            // textBoxPretraga1
            // 
            this.textBoxPretraga1.Location = new System.Drawing.Point(95, 65);
            this.textBoxPretraga1.Name = "textBoxPretraga1";
            this.textBoxPretraga1.Size = new System.Drawing.Size(121, 20);
            this.textBoxPretraga1.TabIndex = 6;
            // 
            // labelPretraga2
            // 
            this.labelPretraga2.AutoSize = true;
            this.labelPretraga2.Location = new System.Drawing.Point(42, 102);
            this.labelPretraga2.Name = "labelPretraga2";
            this.labelPretraga2.Size = new System.Drawing.Size(35, 13);
            this.labelPretraga2.TabIndex = 4;
            this.labelPretraga2.Text = "label3";
            // 
            // labelPretraga1
            // 
            this.labelPretraga1.AutoSize = true;
            this.labelPretraga1.Location = new System.Drawing.Point(42, 72);
            this.labelPretraga1.Name = "labelPretraga1";
            this.labelPretraga1.Size = new System.Drawing.Size(35, 13);
            this.labelPretraga1.TabIndex = 3;
            this.labelPretraga1.Text = "label2";
            // 
            // comboBoxPretraga
            // 
            this.comboBoxPretraga.FormattingEnabled = true;
            this.comboBoxPretraga.Items.AddRange(new object[] {
            "Vozacu",
            "Ceni ",
            "Datumu",
            "Vozilu",
            "Startnoj adresi",
            "Krajnjoj adresi"});
            this.comboBoxPretraga.Location = new System.Drawing.Point(95, 31);
            this.comboBoxPretraga.Name = "comboBoxPretraga";
            this.comboBoxPretraga.Size = new System.Drawing.Size(121, 21);
            this.comboBoxPretraga.TabIndex = 2;
            this.comboBoxPretraga.SelectedIndexChanged += new System.EventHandler(this.comboBoxPretraga_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pretraga po: ";
            // 
            // buttonPretrazi
            // 
            this.buttonPretrazi.Location = new System.Drawing.Point(155, 175);
            this.buttonPretrazi.Name = "buttonPretrazi";
            this.buttonPretrazi.Size = new System.Drawing.Size(75, 23);
            this.buttonPretrazi.TabIndex = 0;
            this.buttonPretrazi.Text = "Pretrazi";
            this.buttonPretrazi.UseVisualStyleBackColor = true;
            this.buttonPretrazi.Click += new System.EventHandler(this.buttonPretrazi_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 577);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.buttonObrisiVoznju);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Taksi sluzba";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView listViewVoznje;
        private System.Windows.Forms.Button buttonDodajVoznju;
        private System.Windows.Forms.Button buttonDodajVozaca;
        private System.Windows.Forms.Button buttonDodajVozilo;
        private System.Windows.Forms.Button buttonObrisiVoznju;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonPretrazi;
        private System.Windows.Forms.ComboBox comboBoxPretraga;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPretraga1;
        private System.Windows.Forms.Label labelPretraga1;
        private System.Windows.Forms.TextBox textBoxPretraga2;
        private System.Windows.Forms.Label labelPretraga2;
        private System.Windows.Forms.DateTimePicker dateTimePickerPretraga2;
        private System.Windows.Forms.DateTimePicker dateTimePickerPretraga1;
    }
}

