﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NPB_Cassandra.DataLayer.QueryEntities;
using NPB_Cassandra.DataLayer;

namespace NPB_Cassandra
{
    public partial class UpdateVoznja : Form
    {
        private Voznja IzabranaVoznja;

        public UpdateVoznja()
        {
            InitializeComponent();
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "MM/dd/yyyy hh:mm";
        }

        public UpdateVoznja(Voznja voznja)
        {
            InitializeComponent();
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "MM/dd/yyyy hh:mm";
            this.Text = this.Text + " " + voznja.ID;
            labelID.Text = voznja.ID.ToString();
            textBoxPolaziste.Text = voznja.Polaziste;
            textBoxOdrediste.Text = voznja.Odrediste;
            textBoxCena.Text = voznja.Cena.ToString();
            dateTimePicker1.Value = voznja.Vreme;
            PopuniComboboxeve(voznja);
            IzabranaVoznja = voznja;
        }

        public void PopuniComboboxeve(Voznja voznja)
        {
            List<Vozac> vozaci = DataProvider.VratiVozace();
            List<Vozilo> vozila = DataProvider.VratiVozila();

            foreach (Vozac vozac in vozaci)
            {
                comboBoxVozac.Items.Add(vozac.ToString());

                if (vozac.Email == voznja.Vozac)
                {
                    comboBoxVozac.SelectedItem = vozac.ToString();
                }
            }

            foreach (Vozilo vozilo in vozila)
            {
                comboBoxVozilo.Items.Add(vozilo.ToString());

                if (vozilo.Tablice == voznja.Vozilo)
                {
                    comboBoxVozilo.SelectedItem = vozilo.ToString();
                }
            }
        }

        private void buttonAzurirajVoznju_Click(object sender, EventArgs e)
        {
            string polaziste = textBoxPolaziste.Text;
            string odrediste = textBoxOdrediste.Text;
            DateTime vreme = dateTimePicker1.Value;
            string cena = textBoxCena.Text;
            var vozac = comboBoxVozac.SelectedItem;
            var vozilo = comboBoxVozilo.SelectedItem;

            if (!Voznja.ProveriPolja(polaziste, odrediste, cena, vreme, vozilo, vozac))
            {
                return;
            }

            Vozilo izabranoVozilo = Vozilo.SelektovanoVozilo(vozilo.ToString());
            Vozac izabranVozac = Vozac.SelektovanVozac(vozac.ToString());

            Voznja voznja = new Voznja(polaziste, odrediste, Int32.Parse(cena), vreme, izabranoVozilo.Tablice, izabranVozac.Email);
            voznja.ID = IzabranaVoznja.ID;

            DataProvider.IzmeniVoznju(voznja);

            MessageBox.Show("Uspesno ste azurirali voznju!");

            this.Close();
        }
    }
}
